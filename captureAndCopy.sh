#!/bin/bash
date=$(date '+%Y-%m-%d %H:%M:%S')
fileOutput="$HOME/Desktop/$date.png"
case $1 in
	screen)
		gnome-screenshot -cf "$fileOutput"
		;;
	window)
		gnome-screenshot -wcf "$fileOutput"
		;;
	area)
		gnome-screenshot -acf "$fileOutput" 
		;;
	*)
		printf "Requires argument either 'screen', 'window', or 'area'\n"
		exit
		;;
esac	

# Check if Screenshot successful
if test -f "$fileOutput"; then 
	printf "Saved new screenshot to \"$fileOutput\"\n"
else
	printf "We didn't quiet catch that screenshot... Try again please!\n"
	exit
fi

cat "$fileOutput" | xclip -i -selection clipboard -target image/png
