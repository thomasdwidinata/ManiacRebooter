# ManiacRebooter 

## Short Description
A initialisation script used to quickly setup my personal Linux Machine after fresh install of my current Linux distro, Ubuntu 20.04 

### Description
A initialisation script used to quickly setup my personal Linux Machine A initialisation script used to quickly setup my personal Linux Machine after fresh install of my current Linux distro, Ubuntu 20.04. I made this in case I've done something wrong with my current daily driver PC and it literally won't boot up or broken (Basically f up) because of my tweaks and stupidity. To overcome this problem, I have made this script that let me reinstall the whole system and have all ready and set up. This script allows me to install my preference apps quickly and configure my shortcuts or configurations without having to type anything.

### Technologies
The technologies, programming language, and operating system that we are using are listed below:
- Ubuntu 20.04.1 LTS
- Bash Script (Actually I do prefer zsh, but bash is the default shell for Ubuntu)

### Installation
Run `chmod +x ./ManiacRebooter.sh` to enable script execution and then run it by typing this command: `./ManiacRebooter.sh`

### My Packages
Below are the packages going to be installed by this script:
- vim
- wget
- curl
- ddcontrol
- gddcontrol
- ddcontrol-db
- i2c-tools
- gparted
- flameshot
- synaptic
- gnome-tweaks
- gnome-shell-extensions
- python3
- python3-pip
- php7.4
- nmap
- zsh
- git
- ibus-pinyin
- ibus-sunpinyin
- exfat-utils
- exfat-fuse
- google-chrome
- minecraft
- steam
- code (Visual Studio Code)
- mockoon
- postman
- discord
- spotify
- arduino
- nodejs
- yarn
- flutter

### Gnome Extensions
Below are the Gnome Extensions going to be installed by this script:
- https://extensions.gnome.org/extension/779/clipboard-indicator/
- https://extensions.gnome.org/extension/945/cpu-power-manager/
- https://extensions.gnome.org/extension/921/multi-monitors-add-on/
- https://extensions.gnome.org/extension/906/sound-output-device-chooser/
- https://extensions.gnome.org/extension/7/removable-drive-menu/
- https://extensions.gnome.org/extension/19/user-themes/
- https://extensions.gnome.org/extension/1251/blyr/
- https://extensions.gnome.org/extension/1131/desk-changer/
- https://extensions.gnome.org/extension/351/icon-hider/

### Additional Setups
This script will also install Powerline Fonts and Oh-my-zsh with its themes. It will also change the theme to `agnoster` and make an alias `alias open="xdg-open"` on `~/.zshrc` because I like MacOS's `open` command and both commands are pretty much similar. There are an additional scripts used to perform screen capture. The dconfs will be fully dumped into system settings based on `default.dconf`.

### Themes
The themes I'm using is (Orchis)[https://github.com/vinceliuice/Orchis-theme] for the GTK and Shell, while using (Tela)[https://github.com/vinceliuice/Tela-icon-theme] for the Icons and (WhiteSur)[https://github.com/vinceliuice/WhiteSur-cursors] for the cursors. Feel free to add any changing wallpapers on `$HOME/Pictures/Wallpaper` as it has been preloaded with one image.

### Future Updates
I may or may not (mayhaps?) update this script extending to multiple choices. There will probably another project used to setup my portable PC (Mac) but I don't know if I still have motivation to do that.
