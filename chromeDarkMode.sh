#!/bin/bash
THEME_MODE=$(gsettings get org.gnome.desktop.interface gtk-theme)
if [[ "$THEME_MODE" == *"light"* ]]
then
	sudo sed -i 's/\/usr\/bin\/google-chrome-stable --force-dark-mode/\/usr\/bin\/google-chrome-stable/' /usr/share/applications/google-chrome.desktop
else
	sudo sed -i 's/\/usr\/bin\/google-chrome-stable/\/usr\/bin\/google-chrome-stable --force-dark-mode/' /usr/share/applications/google-chrome.desktop
fi
