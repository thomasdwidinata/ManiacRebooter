#!/bin/bash

# ManiacRebooter
# Author: Thomas Dwi Dinata <thomasdwidinata@gmail.com> [thomasdinata.dev]
# 
# Version: 0.3 Alpha
# 
# A initialisation script used to quickly setup my personal Linux Machine A initialisation script used to quickly setup my personal Linux Machine after fresh install of my current Linux distro, Ubuntu 20.04. I made this in case I've done something wrong with my current daily driver PC and it literally won't boot up or broken (Basically f up) because of my tweaks and stupidity. To overcome this problem, I have made this script that let me reinstall the whole system and have all ready and set up. This script allows me to install my preference apps quickly and configure my shortcuts or configurations without having to type anything. 
#

printf "ManiacRebooter\n"
printf "By: Thomas Dwi Dinata <thomasdwidinata@gmail.com> [thomasdinata.dev]\n"
printf "Version 0.3 Alpha\n\n"

# Update current system, install drivers, and install packages
echo "Updating current system..."
sudo apt update && sudo apt upgrade -y
echo "Installing proprietary drivers..."
sudo ubuntu-drivers list && sudo ubuntu-drivers install -y
echo "Installing optional packages..."
sudo apt install vim wget curl ddccontrol gddccontrol ddccontrol-db i2c-tools gparted synaptic gnome-tweaks gnome-shell-extensions python3 python3-pip php7.4 nmap zsh git ibus-pinyin ibus-sunpinyin exfat-utils exfat-fuse -y

# Setup ddc-control
echo "Configuring i2c permission settings for DDC..."
sudo adduser $USER i2c
sudo /bin/sh -c 'echo i2c-dev >> /etc/modules'

# Google Chrome
echo "Installing Google Chrome..."
curl https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb --output /tmp/chrome.deb && sudo apt install /tmp/chrome.deb

# Patch Chromium's GPU Renderer upon sleep. Can't believe some apps still need this to work even though Chromium has fixed this bug
echo "#!/bin/sh

set -e

if [ "$2" = "suspend" ] || [ "$2" = "hybrid-sleep" ]
then
    case "$1" in
        pre)
            true
            ;;
        post)
            sleep 1
            pkill -f 'chrome \-\-type=gpu-process'
            ;;
    esac
fi" | sudo tee -a /lib/systemd/system-sleep/revive-chrome-gpu
sudo chmod +x /lib/systemd/system-sleep/revive-chrome-gpu

# Minecraft
echo "Installing Minecraft..."
curl https://launcher.mojang.com/download/Minecraft.deb --output /tmp/minecraft.deb && sudo apt install /tmp/minecraft.deb -y

# Steam
echo "Installing Steam"
wget https://cdn.cloudflare.steamstatic.com/client/installer/steam.deb -O /tmp/steam.deb && sudo apt install /tmp/steam.deb -y

# VSCode
echo "Installing Visual Studio Code..."
if (command -v snap)
then
	sudo snap install code --classic
else
	wget https://go.microsoft.com/fwlink/?LinkID=760868 -O /tmp/vscode.deb && sudo sudo apt install /tmp/vscode.deb -y
fi
echo "Installing Base Visual Studio Code Settings... Be sure to login to Github to fetch latest config or whatever you want to do with it..."
mkdir -p $HOME/.config/Code/User/
cp ./vscode.settings.json $HOME/.config/Code/User/settings.json

# Postman
echo "Installing Postman..."
# The easiest is to get Postman... I can't believe it postman uses tar
if (command -v snap)
then
	sudo snap install postman
elif (command -v flatpak)
then
	flatpak install flathub com.getpostman.Postman
else
	echo "No Snap or Flatpak installed... I guess you are on your own to install Postman..?"
fi

# Mockoon
echo "Installing Mockoon..."
if (command -v snap)
then
    sudo snap install mockoon
else
    echo "Mockoon is not available on flatpak. Using AppImage instead and will be placed under $HOME/Applications"
    mkdir -p $HOME/Applications
    wget https://github.com/mockoon/mockoon/releases/download/v1.12.0/mockoon-1.12.0.AppImage -O $HOME/Applications/Mockoon.AppImage
    chmod +x $HOME/Applications/Mockoon.AppImage
fi

# Flameshot
echo "Installing Flameshot"
if (command -v snap)
then
	sudo snap install flameshot
elif (command -v flatpak)
then
	flatpak install flathub org.flameshot.Flameshot
else
	echo "No Snap or Flatpak installed... I guess you are on your own *again* to install Flameshot..?"
fi
# Setup Flameshot to auto start and hide tray
echo "Configuring Flameshot to hide the tray and auto startup..."
flameshot config -a true -t false

# Discord
echo "Installing Discord..."
wget "https://discord.com/api/download?platform=linux&format=deb" -O /tmp/discord.deb && sudo apt install /tmp/discord.deb -y

# Spotify
echo "Installing Spotify"
if (command -v snap)
then
	sudo snap install spotify
elif (command -v flatpak)
then
	flatpak install flathub com.spotify.Client
else
	echo "No Snap or Flatpak installed... I guess you are on your own *again* to install Spotify..?"
fi

# Arduino
echo "Installing Arduino..."
wget https://downloads.arduino.cc/arduino-1.8.13-linux64.tar.xz -O /tmp/arduino.tar.xz && tar xf /tmp/arduino.tar.xz -C /tmp && mv /tmp/arduino-1.8.13/ /tmp/arduino && sudo mv /tmp/arduino /opt && sudo bash /opt/arduino/install.sh

# Node.js
echo "Installing Node.js 14 LTS..."
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install -y nodejs

# Yarn
echo "Installing Yarn Package Manager..."
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | sudo tee /usr/share/keyrings/yarnkey.gpg >/dev/null
echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn

# Flutter
echo "Installing Flutter using Git..."
(
	cd /opt
	sudo git clone https://github.com/flutter/flutter.git -b stable --depth 1
	sudo chown $USER -R /opt/flutter
	export PATH="${PATH}:/opt/flutter/bin"
	echo "export PATH=\"\${PATH}:/opt/flutter/bin\"" >> ~/.zshrc
	flutter precache
)

# Install Custom Handmade Scripts
echo "Installing Custom Scripts..."
sudo mkdir /opt/sysreq66
sudo cp ./captureAndCopy.sh /opt/sysreq66/
sudo chmod +x /opt/sysreq66/captureAndCopy.sh
sudo cp ./chromeDarkMode.sh /opt/sysreq66/
sudo chmod +x /opt/sysreq66/chromeDarkMode.sh

# Install Themes
echo "Installing Themes..."
echo "Currently using own archive, to get latest theme version, visit:"
printf "\t> https://github.com/vinceliuice/Orchis-theme\n"
printf "\t> https://github.com/vinceliuice/Tela-icon-theme\n"
printf "\t> https://github.com/vinceliuice/WhiteSur-cursors\n"
mkdir -p $HOME/.themes
mkdir -p $HOME/.icons
tar xf ./themes.tar.xz -C $HOME/.themes
tar xf ./icons.tar.xz -C $HOME/.icons

# Gnome Extension
echo "Installing Gnome Extensions..."
GNOME_EXTENSIONS=(
    "https://extensions.gnome.org/extension-data/cpupowermko-sl.de.v23.shell-extension.zip"
    "https://extensions.gnome.org/extension-data/clipboard-indicatortudmotu.com.v34.shell-extension.zip"
    "https://extensions.gnome.org/extension-data/blyryozoon.dev.gmail.com.v8.shell-extension.zip"
    "https://extensions.gnome.org/extension-data/sound-output-device-chooserkgshank.net.v36.shell-extension.zip"
    "https://extensions.gnome.org/extension-data/user-themegnome-shell-extensions.gcampax.github.com.v40.shell-extension.zip"
    "https://extensions.gnome.org/extension-data/drive-menugnome-shell-extensions.gcampax.github.com.v43.shell-extension.zip"
    "https://extensions.gnome.org/extension-data/desk-changereric.gach.gmail.com.v19.shell-extension.zip"
    "https://extensions.gnome.org/extension-data/native-window-placementgnome-shell-extensions.gcampax.github.com.v43.shell-extension.zip"
    "https://extensions.gnome.org/extension-data/hidetopbarmathieu.bidon.ca.v91.shell-extension.zip"
    "https://extensions.gnome.org/extension-data/nightthemeswitcherromainvigier.fr.v46.shell-extension.zip"
)
for EXT in "${GNOME_EXTENSIONS[@]}"
do
    curl $EXT --output "/tmp/ext.zip"
    EXT_UUID="$(unzip -c "/tmp/ext.zip" metadata.json | grep uuid | cut -d \" -f4)"
    mkdir -p ~/.local/share/gnome-shell/extensions/$EXT_UUID
    unzip -q "/tmp/ext.zip" -d ~/.local/share/gnome-shell/extensions/$EXT_UUID
    gnome-extensions enable $EXT_UUID
done
echo "Finished installing extensions. Gnome Extensions will load after reboot"

# Load custom Gnome Dconfs
echo "Loading preset configurations using dconf..."
dconf load / < ./default.dconf
echo "Setting up new default browser to 'Google Chrome'..."
xdg-settings set default-web-browser google-chrome.desktop

# Set vim as default shell editor
sudo update-alternatives --set editor /usr/bin/vim.basic

# Setup Powerline Fonts
echo "Installing additional Powerline Fonts"
git clone https://github.com/powerline/fonts.git --depth=1
cd fonts
./install.sh
cd ..
rm -rf fonts

# Setup ZSH
echo "Installing Oh-My-ZSH with Themes..."
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Replace ZSH Config
echo "Setting up .zshrc..."
sed -i 's/robbyrussell/agnoster/' ~/.zshrc
echo "alias open=\"xdg-open\"" >> ~/.zshrc
# Adding Global Yarn Bin to ZSH
echo "PATH=\"$PATH:$(yarn global bin)\"" >> ~/.zshrc

# Adding Wallpaper for Desk Changer
echo "Adding Wallpapers..."
cp -R ./Wallpaper $HOME/Pictures

# Generate your own SSH Key
echo "Generating SSH Key..."
ssh-keygen -a 256 -t ed25519 -f ~/.ssh/id_$USER

# Disable CUPS Browse Daemon, because who needs this feature
echo "Disabling CUPS Browse Daemon..."
sudo systemctl stop cups-browsed.service
sudo systemctl disable cups-browsed.service

echo "Initial Setup finished!"

# Pre-install User Actions
echo "Some configurations requires user interaction such as setting up crons or other things. Please run these commands only if you know what the command would do. Or else ignore this"
echo "1) Run forceChromeDark.sh to fix Chrome desktop file to force launch using dark mode theme system"
echo "  1> Run 'sudo crontab -e'"
echo "  2> Paste this"
echo "     @reboot /opt/sysreq66/chromeDarkMode.sh"
echo "  3> Save cron file"
echo ""

read -p "Would you like to reboot your system? [y/n]: " -n 1 -r
echo ""
if [[ $REPLY =~ ^[Yy]$ ]];
then
        sudo reboot
else
        echo "Make sure to restart your computer to take effect of newly installed Apps & Drivers."
fi
